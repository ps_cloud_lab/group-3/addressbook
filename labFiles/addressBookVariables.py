

menuSections = {
    "mainMenu" :
    {
        "sectionTitle": "Main Menu",
        "menuItem1": "View All Contacts",
        "menuItem2": "Enter Contact",
        "menuItem3": "Edit Contact",
        "menuItem4": "Delete Contact"
    },
   "editContact" :
    {
        "sectionTitle": "Edit Contact Menu",
        "menuItem1": "Modify Contact"
    },
    "readContacts" :
    {
        "sectionTitle": "Read Contact Menu",
        "menuItem1": "Show All Contacta",
        "menuItem2": "Sort by Last Name",
        "menuItem3": "Sort by contactID" 
    },
    "deleteContact" :
    {
        "sectionTitle": "Delete Contact Menu",
        "menuItem1": "Find Contact"
    }



}