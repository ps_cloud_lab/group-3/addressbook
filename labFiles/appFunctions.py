# addressBook functions
# group 3 project

import json

addressBookJson = "labFiles/addressBook.json"

# heading

def printMenu():
    appBanner ='''

     e            888       888                                      888~~\                    888   _   
    d8b      e88~\888  e88~\888 888-~\  e88~~8e   d88~\  d88~\       888   |  e88~-_   e88~-_  888 e~ ~  
   /Y88b    d888  888 d888  888 888    d888  88b C888   C888         888 _/  d888   i d888   i 888d8b    
  /  Y88b   8888  888 8888  888 888    8888__888  Y88b   Y88b        888  \  8888   | 8888   | 888Y88b   
 /____Y88b  Y888  888 Y888  888 888    Y888    ,   888D   888D       888   | Y888   ' Y888   ' 888 Y88b  
/      Y88b  "88_/888  "88_/888 888     "88___/  \_88P  \_88P        888__/   "88_-~   "88_-~  888  Y88b                                                                                                        
                                                                                                                                                                                                                                            
    '''
 
    print(appBanner)

# print current sectionMenu
#  

# create entry
def createEntry():

    # get data
    firstName = input("Please enter first name: ")
    lastName = input("Please enter last name: ")
    emailAddress = input("Please enter email address: ")
    contactNote = input("Any notes?: ")

    # compile data into dictionary
    newEntry = {
            "firstName": firstName,
            "lastName": lastName,
            "emailAddress": emailAddress,
            "contactNote": contactNote
        }
    
    # show me what you got
    print(newEntry)

# read entry
#   read curred
# edit entry
# delete entry

# utility functions
#   json load current address book



#   json load current address book
def readJsonFile(filename):
    data = ""
    try:
        with open(filename) as json_file :
            data = json.load(json_file)
    except IOError:
        print("Error reading JSON file")
    return data

#   json push entry

# evaluate entry